package vip.yazilim.springboot.basics.componentscan.ctx2;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Context Two
 * 
 * @author Emre Sen
 *
 */
@SpringBootApplication
@ComponentScan("vip.yazilim.springboot.basics.componentscan.ctx1")
public class SpringApplication2 {

}
